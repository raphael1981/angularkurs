import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { MenuComponent } from './menu/menu.component';
import { MainComponent } from './main/main.component';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NotFoundComponent } from './not-found/not-found.component';
import { CategoryModule } from './views/category/category.module';
import {ProductService} from "./services/product/product.service";
import {ToolsModule} from "./tools/tools.module";

/*
Główny moduł aplikacji:
który posiada główny komponent aplikacji 'AppComponent`

*/

@NgModule({
  declarations: [//tutaj deklarujemy komponenty, pipy, dyrektywy zgrupwane w module
    AppComponent, // wpisane w deklarcjach modułu elementy mogą być używane w jego ramach
    MenuComponent,
    MainComponent,
    NotFoundComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    ToolsModule,
    AppRoutingModule, //rozbudowana aplikacja możem mieć wiele podstron czyli routes Moduł AppRoutingModule posiada definicję głównych adresów (podstron)
    HttpClientModule, //oczywiście Modułow z routes może byc wiele ale ten jest główny o czym świaczy RouterModule.forRoot(routes) - użycie 'forRoot'
    BrowserAnimationsModule,
    // CategoryModule,
    // modułów do importowania moze być wiele każdy może zawierać swój zestaw narzędzi w postaci komponetów, pipów, dyretyw oraz serwisy które znajdują sie w 'providers'
    // importując takie moduły importujemy ich zawaratość a narzędzia które są wyeksportwane w tych modułach mozemy uzywać
    // np HttpClientModule pozwala uzywać serwisu HttpClient
    // CommonModule zwiera zestaw podstawowych narzędzi np dyrekywa ngIf czyli warunkowe wyświetlanie elementu czy ngFor pętla tworząca dynamiecznie elementy
  ],
  providers: [
    {provide: Window, useValue: window},
    {provide: Document, useValue: document},
  ], // Tutaj deklarowane są serwisy ale tylko te serwisy które mają w swojej deklaracji ogranicznie
                // czyli nie są oznaczone tak jak niżej dekoratorem który posida właśwość providedIn: 'root' co oznacza że ten serwis nie musi byc
                // impotowany do prowiders bo jest ogolnie dostępny w całej aplikacji, providers umieszcza się serwisy które chcemy ograniczy do jakiegoś modułu
                // @Injectable({
                //   providedIn: 'root',
                // })
  bootstrap: [AppComponent], //właściwość bootstrap używana tylko w jednym module czyli tym głownym zawarty w niej komponent jest głownym komponetem aplikacji
})
export class AppModule {}
