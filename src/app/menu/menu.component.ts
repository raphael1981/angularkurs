import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  DoCheck,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  Renderer2,
  SimpleChanges,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent
  /* implements OnInit, OnChanges, DoCheck, AfterViewInit */
{
  @ViewChild('myBtn') myBtn!: ElementRef;
  @Input() test?: number;
  showMenu = false;
  constructor(
    private changeDetection: ChangeDetectorRef,
    private renderer2: Renderer2,
    private el: ElementRef
  ) {}

  // ngOnInit(): void {
  //   console.log('init');
  //   // console.log(this.myBtn.nativeElement);
  // }
  //
  showMyMenu(): void {
    this.showMenu = !this.showMenu;
  }
  //
  // ngOnChanges(changes: SimpleChanges): void {
  //   console.log(changes);
  // }
  //
  // ngAfterViewInit(): void {
  //   // this.showMenu = true;
  //   console.log(this.el.nativeElement);
  //   this.changeDetection.detectChanges();
  //   this.renderer2.addClass(this.el.nativeElement, 'wild');
  // }
  //
  // ngDoCheck(): void {
  //   console.log('doCheck');
  // }
}
