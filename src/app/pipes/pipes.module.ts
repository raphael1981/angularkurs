import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryChangePipe } from './category-change.pipe';



@NgModule({
  declarations: [
    CategoryChangePipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CategoryChangePipe
  ]
})
export class PipesModule { }
