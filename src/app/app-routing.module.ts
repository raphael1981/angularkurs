import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { CategoriesResolveService } from './services/category/categories-resolve.service';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
  {
    path: '', //głowna strona apliakcji czyli taki HomePage
    pathMatch: 'full', //lub prefix https://qa-stack.pl/programming/42992212/in-angular-what-is-pathmatch-full-and-what-effect-does-it-have
    component: MainComponent, //komponent który wyświeli się w głownej części w znaczniku <router-outlet></router-outlet>
                              //który jest miejscem renderowanie komponentów z routingem
    resolve: { categories: CategoriesResolveService }, //w obiekcie przypisanym do resolve mogą znajdować sie serwisy które pobiorą dane z BE czyli za pomoca
                                                        //żądania GET lub POST w tym przypadku na starcie pobieramy liste kategorii
                                                        //aby wyświeltlić ją w renderowanym komponencie
  },
  {
    path: 'category', //loadChildren to spósb na ładowanie modułu w momencie kiedy jest on rzeczywiscie potrzebny
              //nie musi być on zaimportowany a importuj się w momencie kiedy wchoczimy w adres którego dalsza deklaracja jest w CategoryRoutingModule
    loadChildren: () =>
      import('./views/category/category.module').then((m) => m.CategoryModule),
  },
  // { // inny sposób definiwania bez takzwuaeng lazyloadingu czyli komponent który renderuje się pod route musi być w declarations Modułu
  //   // lub być zaimportowany w module w którym znajduje się w exports czyli staje się nadrzędziem dodanym
  //   path: 'category/:alias',
  //   component: ProductsComponent,
  //   resolve: { products: ProductsResolveService },
  // },
  {path:'**', component: NotFoundComponent}, //symbole wieloznaczny oznacza wysztkie możliwe routes opróczy tych które są wczesnie zdeklarowane jak ten powyżej
                                            // czyli wyłapuje 404 adresy nie zedefiniowane



];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule],
})
export class AppRoutingModule {}
