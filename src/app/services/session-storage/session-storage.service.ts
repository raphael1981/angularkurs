import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SessionStorageService {
  storage: Storage = this.window.sessionStorage;
  storageMap: Map<string, BehaviorSubject<any>> = new Map<
    string,
    BehaviorSubject<any>
  >([]);

  constructor(@Inject(Window) private window: Window) {}

  watch<T>(key: string): Observable<T> {
    if (!this.storageMap.has(key)) {
      this.storageMap.set(key, new BehaviorSubject<any>(null));
    }
    return this.storageMap.get(key)?.asObservable() as Observable<T>;
  }

  setItem<T>(key: string, value: T) {
    let newValue: string =
      typeof value === 'string' ? value : JSON.stringify(value);
    this.storage.setItem(key, newValue);
    if (!this.storageMap.has(key)) {
      this.storageMap.set(key, new BehaviorSubject<any>(null));
    }
    this.storageMap.get(key)?.next(value);
  }

  getItem<T>(key: string): T {
    let value: any = this.storage.getItem(key);
    return typeof value === 'string' ? value : JSON.parse(value);
  }
}
