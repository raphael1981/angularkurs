import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Product} from "../models/product.model";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {SessionStorageService} from "./session-storage/session-storage.service";

@Injectable({
  providedIn: 'root'
})
export class CartService {
  readonly cartStorageKey: string = 'cart';

  constructor(private http: HttpClient, private sessionStorageService: SessionStorageService) { }

  add(product: Product, quantity: number): Observable<any> {
    this.sessionStorageService.setItem<Product>(this.cartStorageKey, product);

    return this.http.post(environment.apiUrl + '/carts', {
      userId: 5,
      date:'2020-02-03',
      products: [{productId: product.id, quantity}]
    })
  }
}
