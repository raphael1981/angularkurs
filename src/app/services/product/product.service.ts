import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpParams,
  HttpParamsOptions,
} from '@angular/common/http';
import { ListParams } from '../../models/list-params.model';
import { Observable } from 'rxjs';
import { Product } from '../../models/product.model';
import { environment } from '../../../environments/environment';
import { defaultParams } from '../../tools/sort-filter/sort-filter.component';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  date = new Date();
  constructor(private http: HttpClient) {}

  getByCategory(
    category: string,
    listParams: ListParams
  ): Observable<Product[]> {
    let params: HttpParams = new HttpParams();
    params = params.append(
      'size',
      String(listParams.size || defaultParams.sort)
    );
    params = params.append('sort', listParams.sort || defaultParams.sort);

    return this.http.get<Product[]>(
      environment.apiUrl + '/products/category/' + category,
      {
        params,
      }
    );
  }

  changeDate() {
    this.date = new Date();
  }
}
