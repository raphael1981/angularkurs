import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Product} from '../../models/product.model';
import {Observable} from 'rxjs';
import {ProductService} from './product.service';
import {SortType} from '../../models/types/sort.type';

@Injectable({
  providedIn: 'root'
})
export class ProductsResolveService implements Resolve<Product[]>{

  constructor(private productService: ProductService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Product[]> {
    const category: string = route.params.alias;
    const size: string = (route.queryParams?.size) ? route.queryParams.size : '10';
    const sort: SortType = (route.queryParams?.sort) ? route.queryParams.sort : 'asc';
    return this.productService.getByCategory(category, {size, sort});
  }
}
