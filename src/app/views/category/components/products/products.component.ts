import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Product } from '../../../../models/product.model';
import {BehaviorSubject, Observable, pipe, Subscription} from 'rxjs';
import { ListParams } from '../../../../models/list-params.model';
import { concatMap } from 'rxjs/operators';
import { ProductService } from '../../../../services/product/product.service';
import {CartService} from "../../../../services/cart.service";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit,OnDestroy {
  queryParams$: Observable<Params> = this.activatedRoute.queryParams;
  products$: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>([]);
  products: Product[] = [];
  alias!: string;

  routeSubscription!: Subscription;
  paramsSubscription!: Subscription;
  addSubcription!: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private productService: ProductService,
    private cartService: CartService
  ) {
    this.products$.next(activatedRoute.snapshot.data.products);
  }

  ngOnInit(): void {
    this.routeSubscription = this.activatedRoute.params.subscribe((params) => {
      this.alias = params.alias;
    });
    this.paramsSubscription = this.queryParams$
      .pipe(
        concatMap((params) =>
          this.productService.getByCategory(this.alias, params)
        )
      )
      .subscribe((products) => {
        this.products$.next(products);
      });
  }

  onFiltersChange(listParams: ListParams): void {
    this.router.navigate(['category', this.alias], { queryParams: listParams });
  }

  onAdd(data: {product: Product | undefined, quantity: number}): void {

    if (data.product) {
      this.addSubcription = this.cartService.add(data.product, data.quantity)
        .subscribe((response)=>{

      })
    }
  }

  ngOnDestroy(): void {
    if(this.routeSubscription) {
      this.routeSubscription.unsubscribe();
    }

    if (this.paramsSubscription) {
     this.paramsSubscription.unsubscribe();
    }

    if (this.addSubcription) {
      this.addSubcription.unsubscribe();
    }
  }
}
