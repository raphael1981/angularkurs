import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from "../../../../../models/product.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  @Input() product?: Product;
  @Output() onAdd: EventEmitter<{product: Product | undefined, quantity: number}> = new EventEmitter<{product: Product | undefined; quantity: number}>();
  form!: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      quantity: [1, [Validators.min(1), Validators.required]]
    })
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.onAdd.emit({product: this.product, quantity: Number(this.form.get('quantity')?.value)});
    }
  }
}
