import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './components/products/products.component';
import {ProductsResolveService} from '../../services/product/products-resolve.service';

const routes: Routes = [
  {
    path: ':alias',
    component: ProductsComponent,
    resolve: {products: ProductsResolveService}
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoryRoutingModule {}
