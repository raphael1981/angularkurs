import { DEFAULT_CURRENCY_CODE, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryRoutingModule } from './category-routing.module';
import { ProductsComponent } from './components/products/products.component';
import { ToolsModule } from '../../tools/tools.module';
import { ProductComponent } from './components/products/product/product.component';
import { PipesModule } from '../../pipes/pipes.module';
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [ProductsComponent, ProductComponent],
  imports: [CommonModule, CategoryRoutingModule, ToolsModule, PipesModule, ReactiveFormsModule, MatInputModule, MatButtonModule],
  providers: [{ provide: DEFAULT_CURRENCY_CODE, useValue: 'EUR' }],
})
export class CategoryModule {}
