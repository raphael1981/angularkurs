import { Component, OnInit } from '@angular/core';
import {SessionStorageService} from "../../services/session-storage/session-storage.service";
import {Observable} from "rxjs";
import {Product} from "../../models/product.model";

@Component({
  selector: 'app-top-cart',
  templateUrl: './top-cart.component.html',
  styleUrls: ['./top-cart.component.scss']
})
export class TopCartComponent implements OnInit {
  cart$: Observable<Product|null> = this.sessionStorageService.watch<Product|null>('cart');
  constructor(private sessionStorageService: SessionStorageService) { }

  ngOnInit(): void {

  }

}
