import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Params } from '@angular/router';
import { ListParams } from '../../models/list-params.model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SortType } from '../../models/types/sort.type';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';

export const defaultParams = {
  size: 10,
  sort: 'asc',
};

type ListQuery = Params | null | ListParams;

@Component({
  selector: 'app-sort-filter',
  templateUrl: './sort-filter.component.html',
  styleUrls: ['./sort-filter.component.scss'],
})
export class SortFilterComponent implements OnInit, OnChanges, OnDestroy {
  @Input() listQueryParams: ListQuery = defaultParams;
  @Output() onChange: EventEmitter<ListParams> = new EventEmitter<ListParams>();
  form!: FormGroup;
  sortTypes: SortType[] = ['asc', 'desc'];
  sizeSubscription?: Subscription;
  sortSubscription?: Subscription;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    console.log('init');
    this.createForm();
  }

  createForm(): void {
    this.form = this.formBuilder.group({
      size: [this.listQueryParams?.size || defaultParams.size],
      sort: [this.listQueryParams?.sort || defaultParams.sort],
    });
    this.sizeSubscription = this.onSizeChange()?.subscribe((value) =>
      this.emitChange()
    );
    this.sortSubscription = this.onSortChange()?.subscribe((value) =>
      this.emitChange()
    );

    // this.form.valueChanges.subscribe((value) => {
    //   console.log(value);
    // });
  }

  emitChange(): void {
    this.onChange.emit(this.form.value);
  }

  onSizeChange(): Observable<number | null> | undefined {
    return this.form
      .get('size')
      ?.valueChanges.pipe(distinctUntilChanged(), debounceTime(1000));
  }

  onSortChange(): Observable<SortType | null> | undefined {
    return this.form.get('sort')?.valueChanges;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes?.listQueryParams && changes.listQueryParams.firstChange) {
      const currentValue: ListQuery = changes.listQueryParams;
      if (currentValue && this.form) {
        this.form
          .get('size')
          ?.setValue(currentValue?.size || defaultParams.size, {
            emitEvent: false,
          });
        this.form
          .get('sort')
          ?.setValue(currentValue?.sort || defaultParams.sort, {
            emitEvent: false,
          });
      }
    }
  }

  ngOnDestroy(): void {
    if (this.sizeSubscription) {
      this.sizeSubscription.unsubscribe();
    }
    if (this.sortSubscription) {
      this.sortSubscription.unsubscribe();
    }
  }
}
