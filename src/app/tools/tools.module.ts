import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SortFilterComponent } from './sort-filter/sort-filter.component';
import {MatSelectModule} from '@angular/material/select';
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import { TopCartComponent } from './top-cart/top-cart.component';

@NgModule({
  declarations: [
    SortFilterComponent,
    TopCartComponent
  ],
  imports: [
    CommonModule,
    MatSelectModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  exports: [
    SortFilterComponent,
    TopCartComponent
  ]
})
export class ToolsModule { }
