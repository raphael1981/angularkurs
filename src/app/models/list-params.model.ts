import { SortType } from './types/sort.type';

export interface ListParams {
  size?: number | string;
  sort?: SortType;
}
