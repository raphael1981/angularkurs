import {Component, OnInit} from '@angular/core';
import {ProductService} from "./services/product/product.service";
import {AsyncSubject, BehaviorSubject, Subject} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{
  title = 'angularApiQApp';
  test = 1;


  constructor(private productService: ProductService) {

  }
  //
  ngOnInit(): void {
    // sessionStorage.setItem('klucz', 'wartosc');
    // console.log(this.productService.date);
    // this.http.get('https://fakestoreapi.com/products')
    //   .toPromise()
    //   .then(response => {
    //     console.log(response);
    //   });

    // const sub1: Subject<{date:Date, nr: number}> = new Subject<{date:Date, nr: number}>();
    // sub1.subscribe((date) => {
    //   console.log('pre', date);
    // })
    // sub1.next({date: new Date(), nr: 1});
    //
    // setTimeout(()=> {
    //   sub1.next({date: new Date(), nr: 2});
    // },2000);
    //
    //
    //
    //
    // sub1.subscribe((date) => {
    //   console.log(date);
    // })


    // const bsub1: BehaviorSubject<{date:Date, nr: number}|null> = new BehaviorSubject<{date:Date, nr: number}|null>(null);
    // bsub1.subscribe((date) => {
    //     console.log('PRE', date);
    // })
    // bsub1.next({date: new Date(), nr: 1});
    //
    // setTimeout(()=> {
    //   bsub1.next({date: new Date(), nr: 2});
    // },6000);
    //
    //
    //
    //
    // bsub1.subscribe((date) => {
    //   console.log('POST', date);
    // })

    // const asub1: AsyncSubject<{date:Date, nr: number}|null> = new AsyncSubject<{date:Date, nr: number}|null>();
    // asub1.subscribe((date) => {
    //     console.log('PRE', date);
    // })
    // asub1.next({date: new Date(), nr: 1});
    //
    // setTimeout(()=> {
    //   asub1.next({date: new Date(), nr: 2});
    // },2000);
    //
    //
    // asub1.subscribe((date) => {
    //   console.log('POST', date);
    // })
    //
    // setTimeout(()=> {
    //   asub1.complete();
    // },6000)
    //
    //
    // setTimeout(()=> {
    //   asub1.next({date: new Date(), nr: 3});
    // },8000)
  }

  // ngOnInit(): void {
  //   fetch('https://fakestoreapi.com/products')
  //     .then((data: Response) => data.json())
  //     .then(data => console.log(data));
  // }

  change(): void {
    this.productService.changeDate();
  }
}
